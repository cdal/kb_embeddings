# kb_embeddings
Code for weighted DeepWalk and weighted TransE - KB embeddings

Paper: Adverse Drug Event Predictionusing Noisy Literature-Derived Knowledge Graphs: Algorithm Development and Evaluation

This code repo has the following folders:

./

|

-- weighted_DeepWalk

|

-- weighted_transE

|

-- experiments

weighted_DeepWalk - folder that helps generate weighted DeepWalk and DeepWalk embeddings for a dataset

weighted_transE - folder that helps generate weighted TransE and TransE embeddings for a dataset

experiments - folder containing both adverse side effect prediction and polypharmacy experiments for evaluating representations learnt
