This folder contains the code needed to generate DeepWalk and weighted DeepWalk embeddings from the dataset passed in.

Reference paper: Adverse Drug Event Prediction using Noisy Literature-Derived Knowledge Graphs (Abel Lim, Ragunathan Mariappan, Vaibhav Rajan)

Training:
========

##### Environment Setup:

Run the following commands to setup the environment needed to run weighted DeepWalk and DeepWalk.

```
conda env create -f environment.yml

conda activate weighted_deepwalk
```

##### Generating random walks:

Run the following command, after updating the values in [] as needed:

```
cd ./scripts/

python generate_rand_walks.py 
--data_file [data file name] 
--output_file [random walk output name prefix] 
--n [number of files to generate, used for repeating experiments] 
--num_walks 20 
--walk_length 1000 
--add_edges
--sample_method [randu|attrs]
```

Use "randu" as the sample_method for normal DeepWalk and "attrs" as the sample_method for weighted DeepWalk.

data_file refers to the path where the csv containing triplets of the form (subject, predicate, object) are placed.

##### Representation learning:

This is done based on the original word2vec implementation from https://github.com/tmikolov/word2vec

Run the following commands to generate embeddings:

```
cd ./word2vec-master/

gcc word2vec.c -o word2vec -lm -pthread -O3 -march=native -funroll-loops

./word2vec -train [random walk output] -output [embeddings output name] -window 10 -size 256 -sample 1e-5 -iter 5 -alpha 0.025 -threads 4
```

Set [random walk output] to the file generated in the random walk generation step above, i.e. "../scripts/<filename>"

