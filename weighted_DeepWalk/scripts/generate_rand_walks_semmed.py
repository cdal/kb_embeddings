from __future__ import print_function

import argparse
import csv
from queue import Queue

import os
import threading

import random

import numpy as np

import time

import sklearn
from scipy.special import softmax

KEY_SUBJECT_CUI = "SUBJECT_CUI"
KEY_SUBJECT_NAME = "SUBJECT_NAME"
KEY_SUBJECT_TYPE = "SUBJECT_SEMTYPE"

KEY_OBJECT_CUI = "OBJECT_CUI"
KEY_OBJECT_NAME = "OBJECT_NAME"
KEY_OBJECT_TYPE = "OBJECT_SEMTYPE"

KEY_PREDICATE = "PREDICATE"
KEY_OCCURS = "OCCURS"
KEY_SUBJECT_SCORE = "SUBJECT_SCORE"
KEY_OBJECT_SCORE = "OBJECT_SCORE"


parser = argparse.ArgumentParser()
parser.add_argument(
    '--data_file',
    type=str,
    default=None,
    help='File name of data file')
parser.add_argument(
    '--n',
    type=int,
    default=1,
    help='Number of outputs to generate')
parser.add_argument(
    '--num_walks',
    type=int,
    default=1,
    help='Number of walks per node to generate')
parser.add_argument(
    '--walk_length',
    type=int,
    default=100,
    help='Length of each walk')
parser.add_argument(
    '--output_file',
    type=str,
    default=None,
    help='Basename of output file, without extension')
parser.add_argument(
    '--add_edges',
    action='store_true',
    help='Whether to add edges when generating walks. '
         'If true, walk_length counts added edges')
parser.add_argument(
    '--sample_method',
    type=str,
    default="randu",
    help='Which sampling method to use for choosing next node'
         'randu = random uniform on neighbors'
         'attrs = attribute-based distribution (add_edges must be true)')
FLAGS, unparsed = parser.parse_known_args()

# unused
class MetaPathSchema:
    def __init__(self, schema_list):
        self.schema_list = schema_list
        self.index = 0

    def reset(self, idx):
        self.index = idx

    def next_type(self):
        result = self.schema_list[self.index % len(self.schema_list)]
        self.index += 1
        return result


class Edge:
    def __init__(self, predicate):
        self.predicate = predicate

    def __hash__(self):
        return hash(self.predicate)

    def __eq__(self, other):
        return self.predicate == other.predicate

    def __str__(self):
        return '_'.join(["PREDICATE", self.predicate])


class Node:
    def __init__(self, cui, name):
        self.cui = cui
        self.name = name
        self.types = set()

    def __hash__(self):
        return hash(self.cui)

    def __eq__(self, other):
        return self.cui == other.cui

    def add_type(self, type):
        self.types.add(type)

    def __str__(self):
        return '|'.join(self.types) + '_' + self.cui + '_' + self.name


class RandomWalkGenerator:

    def __init__(self, add_edges, sample_method):
        # Stored by read data
        self.add_edges = add_edges
        self.sample_method = sample_method

        self.cui_to_node = {}
        self.pred_to_edge = {}

        self.triplets_to_occurs = {}
        self.triplets_to_scores = {}

        self.pred_objects = []
        self.pred_objects_to_id = {}
        self.subjects_to_neighbors = {}
        self.subjects_to_occurs = {}
        self.subjects_to_scores = {}

        self.sample_weights_lock = threading.Lock()
        self.subject_to_sample_weights = {}

        self.walk_write_lock = threading.Lock()
        self.num_walks_generated = None
        self.total_walks_to_gen = None

    def add_node(self, cui, name, type):
        if cui not in self.cui_to_node:
            self.cui_to_node[cui] = Node(cui, name)
        self.cui_to_node[cui].add_type(type)
        return cui

    def add_edge(self, predicate):
        if predicate not in self.pred_to_edge:
            self.pred_to_edge[predicate] = Edge(predicate)
        return predicate

    def add_pred_object(self, predicate, object):
        if (predicate, object) not in self.pred_objects_to_id:
            self.pred_objects.append((predicate, object))
            self.pred_objects_to_id.update(
                {(predicate, object): len(self.pred_objects) - 1})
        return self.pred_objects_to_id[(predicate, object)]

    def validate(self, row):
        for entry in [row[KEY_SUBJECT_CUI], row[KEY_SUBJECT_TYPE],
                      row[KEY_OBJECT_CUI], row[KEY_OBJECT_TYPE]]:
            if ' ' in entry:
                return False
        return True

    def sample_next(self, last_object):
        if self.sample_method == "randu" and not self.add_edges:
            next_object_idx = random.randint(0, len(self.subjects_to_neighbors[last_object]) - 1)
            next_object = self.subjects_to_neighbors[last_object][next_object_idx]
            sampled_words = [str(self.cui_to_node[next_object])]
            return next_object, sampled_words

        if self.sample_method == "randu" and self.add_edges:
            pred_obj_idx = random.randint(0, len(self.subjects_to_neighbors[last_object]) - 1)
            pred_obj_idx = self.subjects_to_neighbors[last_object][pred_obj_idx]
            predicate, next_object = self.pred_objects[pred_obj_idx]
            sampled_words = [str(self.pred_to_edge[predicate]),
                             str(self.cui_to_node[next_object])]
            return next_object, sampled_words

        if not self.add_edges or self.sample_method != "attrs":
            print("Unsupported sample method")
            exit(1)

        # self.sample_weights_lock.acquire()
        if last_object not in self.subject_to_sample_weights:
            num_neighbors = len(self.subjects_to_neighbors[last_object])
            neighbor_occurs = np.empty((num_neighbors,))
            neighbor_subj_score = np.empty((num_neighbors,))
            neighbor_obj_score = np.empty((num_neighbors,))

            for idx, neighbor in enumerate(self.subjects_to_neighbors[last_object]):
                predicate, object = self.pred_objects[neighbor]
                triplet = (predicate, last_object, object)
                neighbor_occurs[idx] = self.triplets_to_occurs[triplet]
                neighbor_subj_score[idx], neighbor_obj_score[idx] \
                    = self.triplets_to_scores[triplet]

            # weights = (neighbor_occurs / np.max(neighbor_occurs)) \
            #           * neighbor_subj_score / np.max(neighbor_subj_score) \
            #           * neighbor_obj_score / np.max(neighbor_obj_score)
            weights = (neighbor_occurs / np.max(neighbor_occurs))
            weights = np.cumsum(softmax(weights))
            self.subject_to_sample_weights[last_object] = weights
        # self.sample_weights_lock.release()

        next_pred_idx = random.choices(self.subjects_to_neighbors[last_object],
                                       cum_weights=self.subject_to_sample_weights[last_object],
                                       k=1)[0]

        # rand_float = random.random()
        # cum_probs = 0.0
        # random_neighbor_idx  = -1
        # for idx in range(len(self.subject_to_sample_weights[last_object])):
        #     cum_probs += self.subject_to_sample_weights[last_object][idx]
        #     if rand_float < cum_probs:
        #         random_neighbor_idx = idx
        #         break

        # random_neighbor_idx = np.random.choice(
        #     a=len(self.subjects_to_neighbors[last_object]),
        #     p=self.subject_to_sample_weights[last_object],
        #     replace=False,
        #     size=1)[0]
        # random_neighbor_idx = random.randint(0, len(self.subjects_to_neighbors[last_object]) - 1)

        # next_pred_idx = self.subjects_to_neighbors[last_object][random_neighbor_idx]
        predicate, next_object = self.pred_objects[next_pred_idx]
        sampled_words = [
            str(self.pred_to_edge[predicate]),
            str(self.cui_to_node[next_object])]
        return next_object, sampled_words

    def read_data(self, data_file):
        print("reading data from: ", data_file)
        time_start = time.time()
        with open(data_file) as f:
            reader = csv.DictReader(f)
            for row in reader:
                if not self.validate(row):
                    continue

                subject = self.add_node(
                    cui=row[KEY_SUBJECT_CUI],
                    name=row[KEY_SUBJECT_NAME].replace(' ', ''),
                    type=row[KEY_SUBJECT_TYPE])
                object = self.add_node(
                    cui=row[KEY_OBJECT_CUI],
                    name=row[KEY_OBJECT_NAME].replace(' ', ''),
                    type=row[KEY_OBJECT_TYPE])
                predicate = self.add_edge(predicate=row[KEY_PREDICATE])

                if subject not in self.subjects_to_neighbors:
                    self.subjects_to_neighbors[subject] = set()

                if self.add_edges:
                    pred_obj_idx = self.add_pred_object(predicate, object)
                    self.subjects_to_neighbors[subject].add(pred_obj_idx)
                else:
                    self.subjects_to_neighbors[subject].add(object)

                # Additional metadata
                occurs = int(row[KEY_OCCURS])
                subject_score = int(row[KEY_SUBJECT_SCORE])
                object_score = int(row[KEY_OBJECT_SCORE])

                triplet = (predicate, subject, object)
                if triplet not in self.triplets_to_occurs:
                    self.triplets_to_occurs[triplet] = occurs
                if triplet not in self.triplets_to_scores:
                    self.triplets_to_scores[triplet] = []
                self.triplets_to_scores[triplet].append(np.asarray([subject_score, object_score]))

            # Convert set into list
            for subject in self.subjects_to_neighbors:
                self.subjects_to_neighbors[subject] = list(self.subjects_to_neighbors[subject])

            # total occurs already accounted for triplets in input file
            # average scores are not, need to compute for each triplet
            for triplet in self.triplets_to_scores:
                self.triplets_to_scores[triplet] = np.stack(self.triplets_to_scores[triplet])
                self.triplets_to_scores[triplet] = np.mean(self.triplets_to_scores[triplet], 0)

            print("Finished reading data in %.4fs" % (time.time() - time_start))
            print("### Total number of subjects: ", len(self.subjects_to_neighbors))
            if self.add_edges:
                print("### Total number of predicate, objects: ", len(self.pred_objects))

    def _generate_thread(self, walk_length, out_name, id, num_threads):
        walk_times = Queue(50)
        for idx, initial_subject in enumerate(self.subjects_to_neighbors):
            time_start = time.time()
            last_object = initial_subject
            outline = [str(self.cui_to_node[last_object])]
            while len(outline) < walk_length:
                if last_object not in self.subjects_to_neighbors:
                    break
                last_object, sampled_words = self.sample_next(last_object)
                outline.extend(sampled_words)
            walk_time = time.time() - time_start
            if walk_times.full():
                walk_times.get()
            walk_times.put(walk_time)
            average_walk_time = np.mean(list(walk_times.queue))

            # self.walk_write_lock.acquire()
            with open(out_name, "a") as f:
                f.write(' '.join(outline) + "\n")
            self.num_walks_generated += 1
            if self.num_walks_generated % (0.1 * self.total_walks_to_gen) == 0:
                progress = 100 * float(self.num_walks_generated) / float(self.total_walks_to_gen)
                print("ID:", id,
                      "| Progress: %.2f%%" % progress,
                      "| ETA: %.5fmins" % (average_walk_time / num_threads / 60
                                           * (self.total_walks_to_gen - self.num_walks_generated)),
                      "| Avg walk time: %.5fs" % average_walk_time,
                      "| Walks generated:", self.num_walks_generated)
            # self.walk_write_lock.release()

    def generate(self, out_name, num_walks, walk_length):
        print("Generating random paths and saving to ", out_name)
        self.num_walks_generated = 0
        self.total_walks_to_gen = len(self.subjects_to_neighbors) * num_walks
        threads  = []
        for walk_id in range(0, num_walks):
            threads.append(threading.Thread(target=self._generate_thread,
                                            args=[walk_length, out_name, walk_id, num_walks]))
        for thread in threads:
            thread.start()
        for thread in threads:
            thread.join()


def main():
    output_file = ".".join([FLAGS.output_file, str(0), "txt"])
    if os.path.exists(output_file):
        print("Output file", FLAGS.output_file, "already exists, exiting")
        exit(1)

    generator = RandomWalkGenerator(FLAGS.add_edges, FLAGS.sample_method)
    generator.read_data(FLAGS.data_file)

    generator.generate(output_file, FLAGS.num_walks, FLAGS.walk_length)

if __name__ == "__main__":
    main()
