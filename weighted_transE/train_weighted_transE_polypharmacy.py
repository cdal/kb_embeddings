#!/usr/bin/env python
# coding: utf-8

# In[1]:

# In google colab:
# !pip install tensorflow==1.15.0
# !git clone -b OpenKE-Tensorflow1.0 --single-branch https://github.com/thunlp/OpenKE.git
# %cd OpenKE
# !bash make.sh


#get_ipython().system('pip install tensorflow==1.15.0')
#get_ipython().system('git clone -b OpenKE-Tensorflow1.0 --single-branch https://github.com/thunlp/OpenKE.git')
#get_ipython().run_line_magic('cd', 'OpenKE')
#get_ipython().system('bash make.sh')




# In[2]:


import config
import models
import tensorflow as tf
import numpy as np
import os
os.environ['CUDA_VISIBLE_DEVICES']='1'
#Input training files from benchmarks/FB15K/ folder.
con = config.Config()
#True: Input test files from the same folder.
con.set_in_path("./benchmarks/polypharmacy/")
con.set_test_link_prediction(False)
con.set_test_triple_classification(False)
con.set_work_threads(8)
con.set_train_times(100)
con.set_nbatches(512)
con.set_alpha(0.001)
con.set_margin(1.0)
con.set_bern(0)
con.set_dimension(100)
con.set_ent_neg_rate(1)
con.set_rel_neg_rate(0)
con.set_opt_method("SGD")

#Models will be exported via tf.Saver() automatically.
con.set_export_files("./res/Polypharmacymodel.vec.tf", 0)
#Model parameters will be exported to json files automatically.
con.set_out_files("./res/Polypharmacyembedding.vec.json")
#Initialize experimental settings.
con.init()
#Set the knowledge embedding model
con.set_model(models.weighted_TransE)
#con.set_model(models.TransE)
#Train the model.
con.run()

embeddings = con.get_parameters("numpy")
np.save('polypharmacy_Embeddings_weighted_transE.npy',embeddings)

