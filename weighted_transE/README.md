This folder contains code that generates transE and weighted transE embeddings for the dataset passed.

Reference for TransE: OpenKE: An Open Toolkit for Knowledge Embedding (Han, Xu and Cao, Shulin and Lv, Xin and Lin, Yankai and Liu, Zhiyuan and Sun, Maosong and Li, Juanzi)

Training:
========

##### Environment Setup:

Run the following commands to setup a Conda environment to generate TransE and weighted TransE representations:

```
conda env create -f environment.yml

conda activate kb_embeddings
```

##### Representation Learning:

1. Place the files entity2id.txt, relation2id.txt, train2id.txt and triplets.csv in the path ./benchmarks/<dataset name>.
2. Create a file train_transE_dataset.py and update the hyperparameters as needed. Set con.set_in_path to refer to ./benchmarks/<dataset name> and con.set_model to models.TransE for normal TransE and models.weighted_TransE for weighted TransE.
3. Execute the command:
```
python train_transE_dataset.py
```

The embeddings will be saved in the present working directory.

