import pandas as pd
import numpy as np
import random
import itertools

# Polypharmacy dataset path
polypharmacy_data_path = "../data/"
polypharmacy_train_ddi_pos_file = polypharmacy_data_path + "decagon_split_train_ddi.tsv"
polypharmacy_test_ddi_pos_file = polypharmacy_data_path + "decagon_split_test_ddi_pos.tsv"
polypharmacy_test_ddi_neg_file = polypharmacy_data_path + "decagon_split_test_ddi_neg.tsv"
polypharmacy_val_ddi_pos_file = polypharmacy_data_path + "decagon_split_val_ddi_pos.tsv"
polypharmacy_val_ddi_neg_file = polypharmacy_data_path + "decagon_split_val_ddi_neg.tsv"

# data files mapping entity to id, relation to id
data_files_path = "../../../weighted_transE/benchmarks/polypharmacy/"
entity2id_file = data_files_path + "entity2id.txt"
relation2id_file = data_files_path + "relation2id.txt"

rng = 42
random.seed(rng)

# load entities and relations
entity_df = pd.read_csv(entity2id_file, sep = "\t", header = None, skiprows = [0])
entity_df.columns = ["entity", "id"]

relation_df = pd.read_csv(relation2id_file, sep = "\t", header = None, skiprows = [0])
relation_df.columns = ["relation", "id"]

# loading all drug and side effect names
drugs_df = pd.read_csv("../data/drug_names.csv", header = None)
drugs_df.columns = ["drugID", "drug"]
drugs_df['drug'] = drugs_df["drug"].apply(lambda x: x.replace(" ", "_").lower())

drug_names = drugs_df.set_index('drugID').to_dict()['drug']

side_effect_names_df = pd.read_csv("../data/side_effect_names.tsv", sep='\t', header=None)
side_effect_names_df.columns = ["side_effect_ID", "side_effect"]

side_effect_names_df["side_effect"] = side_effect_names_df["side_effect"].apply(lambda x: x.replace(" ", "_").upper())

se_names = side_effect_names_df.set_index("side_effect_ID").to_dict()["side_effect"]

resolve_drug_id = lambda drug_id: drug_names[drug_id] if drug_id in drug_names.keys() else drug_id
resolve_side_effect_id = lambda side_effect_id: se_names[side_effect_id[:-2]]+"-2" if side_effect_id[-2:]=="-2" else se_names[side_effect_id]

def resolve_names_ddi_df(df):
    df['drugA'] = df["drugA_ID"].apply(resolve_drug_id)
    df['sideeffect'] = df["sideeffect_ID"].apply(resolve_side_effect_id)
    df['drugB'] = df['drugB_ID'].apply(resolve_drug_id)


train_ddi_df = pd.read_csv(polypharmacy_train_ddi_pos_file, header = None, delimiter = "\t")
train_ddi_df.columns = ["drugA_ID", "sideeffect_ID", "drugB_ID"]
print("Train DDI pos df:")
print(train_ddi_df.head())
print(train_ddi_df.shape)

resolve_names_ddi_df(train_ddi_df)

train_ddi_df = train_ddi_df.merge(entity_df, left_on = "drugA", right_on = "entity")
train_ddi_df.drop(["entity"], inplace = True, axis = 1)
train_ddi_df.rename(columns = {"id": "drugA_id"}, inplace = True)
train_ddi_df = train_ddi_df.merge(entity_df, left_on = "drugB", right_on = "entity")
train_ddi_df.drop(["entity"], inplace = True, axis = 1)
train_ddi_df.rename(columns = {"id": "drugB_id"}, inplace = True)
train_ddi_df = train_ddi_df.merge(relation_df, left_on = "sideeffect", right_on = "relation")
train_ddi_df.drop(["relation"], inplace = True, axis = 1)
train_ddi_df.rename(columns = {"id": "se_id"}, inplace = True)

# Load test pos and test neg df
test_ddi_pos_df = pd.read_csv(polypharmacy_test_ddi_pos_file, header = None, delimiter = "\t")
test_ddi_pos_df.columns = ["drugA_ID", "sideeffect_ID", "drugB_ID"]
resolve_names_ddi_df(test_ddi_pos_df)
test_ddi_pos_df = test_ddi_pos_df.merge(entity_df, left_on = "drugA", right_on = "entity")
test_ddi_pos_df.drop(["entity"], inplace = True, axis = 1)
test_ddi_pos_df.rename(columns = {"id": "drugA_id"}, inplace = True)

test_ddi_pos_df = test_ddi_pos_df.merge(entity_df, left_on = "drugB", right_on = "entity")
test_ddi_pos_df.drop(["entity"], inplace = True, axis = 1)
test_ddi_pos_df.rename(columns = {"id": "drugB_id"}, inplace = True)

test_ddi_pos_df = test_ddi_pos_df.merge(relation_df, left_on = "sideeffect", right_on = "relation")
test_ddi_pos_df.drop(["relation"], inplace = True, axis = 1)
test_ddi_pos_df.rename(columns = {"id": "se_id"}, inplace = True)
print("Test DDI pos df:")
print(test_ddi_pos_df.head())

test_ddi_neg_df = pd.read_csv(polypharmacy_test_ddi_neg_file, header = None, delimiter = "\t")
test_ddi_neg_df.columns = ["drugA_ID", "sideeffect_ID", "drugB_ID"]
resolve_names_ddi_df(test_ddi_neg_df)

test_ddi_neg_df = test_ddi_neg_df.merge(entity_df, left_on = "drugA", right_on = "entity")
test_ddi_neg_df.drop(["entity"], inplace = True, axis = 1)
test_ddi_neg_df.rename(columns = {"id": "drugA_id"}, inplace = True)

test_ddi_neg_df = test_ddi_neg_df.merge(entity_df, left_on = "drugB", right_on = "entity")
test_ddi_neg_df.drop(["entity"], inplace = True, axis = 1)
test_ddi_neg_df.rename(columns = {"id": "drugB_id"}, inplace = True)

test_ddi_neg_df = test_ddi_neg_df.merge(relation_df, left_on = "sideeffect", right_on = "relation")
test_ddi_neg_df.drop(["relation"], inplace = True, axis = 1)
test_ddi_neg_df.rename(columns = {"id": "se_id"}, inplace = True)
print("Test DDI neg df:")
print(test_ddi_neg_df.head())

test_ddi_df = pd.concat([test_ddi_pos_df, test_ddi_neg_df], ignore_index = True)

train_ddi_df["triplets"] = train_ddi_df.apply(lambda row: (row["drugA"], row["sideeffect"], row["drugB"]), axis = 1)
test_ddi_df["triplets"] = test_ddi_df.apply(lambda row: (row["drugA"], row["sideeffect"], row["drugB"]), axis = 1)

all_triplets = train_ddi_df["triplets"].tolist() + test_ddi_df['triplets'].tolist()

train_pos_dist_dict = train_ddi_df.groupby("drugA").count().to_dict()["drugA_ID"]

# random sampling of drugs and side effects
drugA_unique = list(train_ddi_df.drugA.unique())
drugB_unique = list(train_ddi_df.drugB.unique())
se_unique = list(train_ddi_df.sideeffect.unique())
# Uncomment below lines only for first exec
train_ddi_neg_df = pd.DataFrame()
#for drug in drugA_unique:
#    possible_se = random.sample(se_unique, min(train_pos_dist_dict[drug], len(se_unique)))
#    possible_drugB = random.sample(drugB_unique, min(train_pos_dist_dict[drug], len(drugB_unique)))
#    for i in possible_se:
#        for j in possible_drugB:
#            if (drug, i, j) not in all_triplets:
#                print(f"({drug}, {i}, {j})")
#                train_ddi_neg_df = train_ddi_neg_df.append({"drugA": drug, "sideeffect": i, "drugB": j}, ignore_index = True)
train_ddi_neg_triplets = []
#for drug in drugA_unique:
#    for count in range(train_pos_dist_dict[drug]):
#for drug, v in train_pos_dist_dict.items():
#        possible_se = random.sample(se_unique, min(v, len(se_unique)))
#        possible_drugB = random.sample(drugB_unique, min(v, len(drugB_unique)))
cartesian_product = list(itertools.product(drugA_unique, se_unique, drugB_unique))
all_neg_triplets = list(set(cartesian_product) - set(all_triplets))
sample = random.sample(all_neg_triplets, 7323790) # 1000 is a buffer, 7323790 is the number of samples in positive df
#for r in sample:
#    if (r[0], r[1], r[2]) not in all_triplets and (r[0], r[1], r[2]) not in train_ddi_neg_triplets:
#    print(f"({r[0]}, {r[1]}, {r[2]})")
#    train_ddi_neg_df = train_ddi_neg_df.append({"drugA": r[0], "sideeffect": r[1], "drugB": r[2]}, ignore_index = True)
#    train_ddi_neg_triplets.append((r[0], r[1], r[2]))

train_ddi_neg_df["samples"] = sample
train_ddi_neg_df["drugA"] = train_ddi_neg_df["samples"].apply(lambda x: x[0])
train_ddi_neg_df["sideeffect"] = train_ddi_neg_df["samples"].apply(lambda x: x[1])
train_ddi_neg_df["drugB"] = train_ddi_neg_df["samples"].apply(lambda x: x[2])

print("Train DDI neg df:")
print(train_ddi_neg_df.head())
print(train_ddi_neg_df.shape)

train_ddi_neg_df[["drugA", "sideeffect", "drugB"]].to_csv(polypharmacy_data_path + "decagon_split_train_ddi_neg.tsv", sep = "\t", index = False)
print("Exec done!")


