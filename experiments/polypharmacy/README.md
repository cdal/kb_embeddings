Instructions on reproducing the Polypharmacy experiment:
=======================================================

#### Environment Setup:

Run the following commands to setup an environment to run the Polypharmacy experiment:

```
conda env create -f environment.yml

conda activate predicting_ddis_with_esp_env
```

#### Dataset:

The dataset used here is the same as the one from the paper Predicting Adverse Drug-Drug Interactions with Neural Embedding of Semantic Predications, Burkhardt et al.. The original dataset from the paper is available at https://zenodo.org/record/3333834/files/esp_ddi_zenodo.tar.gz. In our experiment, we augmented the dataset to include negative drug-drug interaction triplets, for use in the downstream link prediction task. The representation learning was done on the same dataset as the one in the original paper.

Download the dataset and extract it to the appropriate location as follows:

```
cd ./data/

wget <link to data tar file>

tar -xvf <data tar file>
```

Ensure the following files are present after extraction:

```
user@hostname:~/kb_embeddings/experiments/polypharmacy$ ll data
total 732472
drwxrwxr-x 2 user user       4096 Jul 26 22:11 ./
drwxrwxr-x 5 user user       4096 Jul 28 11:06 ../
-rw-r--r-- 1 user user   32918112 Jul 20  2019 decagon_split_test_ddi_neg.tsv
-rw-r--r-- 1 user user   32918112 Jul 20  2019 decagon_split_test_ddi_pos.tsv
-rw-rw-r-- 1 user user  291946065 Jun  6 12:42 decagon_split_train_ddi_neg.tsv
-rw-r--r-- 1 user user  263656440 Jul 20  2019 decagon_split_train_ddi.tsv
-rw-r--r-- 1 user user     812731 Jul 20  2019 decagon_split_train_drug_protein.tsv
-rw-r--r-- 1 user user   61561611 Jul 20  2019 decagon_split_train_ppi.tsv
-rw-r--r-- 1 user user   32918112 Jul 20  2019 decagon_split_val_ddi_neg.tsv
-rw-r--r-- 1 user user   32918112 Jul 20  2019 decagon_split_val_ddi_pos.tsv
-rw-rw-r-- 1 user user      16064 May 27 10:47 drug_names.csv
-rw-rw-r-- 1 user user 8143979811 Jul 26 12:27 PREDICATIONS_OCCURS.csv
-rw-rw-r-- 1 user user        332 Jul 26 11:12 README.md
-rw-rw-r-- 1 user user     349768 May 27 10:47 side_effect_names.tsv
```

#### Data Preparation for representation learning:

##### DeepWalk and weighted DeepWalk:

DeepWalk and weighted DeepWalk involves two stages - generation of random walks and representation learning based on the walks generated. The dataset downloaded needs to be converted into a csv file having features subject_id, subject name, subject_type, object_id, object name, object_type, predicate, occurence score for (subject, object), object score and subject scores.

###### Experiment setting 1:

For the polypharmacy dataset above, subject scores and object scores are set to 1000 and occurence score is calculated based on how many times a subject and object occur together in a triplet. To generate this csv file (decagon.csv), execute the following steps:

1. Open the notebook at the location ./data_prep/weighted_DeepWalk/data_prep_weighted_DeepWalk_polypharmacy.ipynb
2. Ensure the path references mentioned in the notebook have all the necessary files.
3. Execute the entire notebook.

The resulting csv file would be saved at the location ~/kb_embeddings/weighted_DeepWalk/dataset/decagon.csv

###### Experiment setting 2:

For the polypharmacy and SemMedDB dataset, a combined csv file is generated. For the triplets from polypharmacy, the subject and object scores are set to 1000(maximum from SemMedDB) and occurence to 33478(maximum from SemMedDB). To generate the csv file (combined.csv), execute the following steps:

1. Open the notebook at the location ./data_prep/weighted_DeepWalk/data_prep_weighted_DeepWalk_combined.ipynb
2. Ensure the path references mentioned in the notebook have all the necessary files.
3. Execute the entire notebook.

The resulting csv file would be saved at the location ~/kb_embeddings/weighted_DeepWalk/dataset/combined.csv

##### TransE and weighted TransE:

TransE and weighted TransE require the files entity2id.txt(maps each entity/node of the triplets to a unique id), relation2id.txt(maps each relation/edge of a triplet to a unique id), train2id.txt(maps each triplet to its corresponding entity and relation id).

For weighted TransE, an additional triplets.csv file is also generated, that maps each triplet to its weight. The weight is calculated as the ratio of (subject score * occurence score * object score of the triplet) to (maximum subject score * maximum object score * maximum occurence score of the dataset).

###### Experiment setting 1:

To generate the relevant files for Polypharmacy dataset, execute the following steps:

1. Open the notebook at the location ./data_prep/weighted_TransE/data_prep_weighted_TransE_polypharmacy.ipynb
2. Ensure the path references mentioned in the notebook have all the necessary files.
3. Execute the entire notebook.

The resulting files would be saved at the location ~/kb_embeddings/weighted_transE/benchmarks/polypharmacy/

###### Experiment setting 2:

To generate the relevant files for Polypharmacy and SemMedDB dataset, execute the following steps:

1. Open the notebook at the location ./data_prep/weighted_TransE/data_prep_weighted_TransE_combined.ipynb
2. Ensure the path references mentioned in the notebook have all the necessary files.
3. Execute the entire notebook.

The resulting files would be saved at the location ~/kb_embeddings/weighted_transE/benchmarks/combined/

#### Representation Learning:

##### DeepWalk and weighted DeepWalk:

###### Stage 1 - Random Walk generation:

To generate random walks for the dataset, execute the following commands:

```
cd ~/kb_embeddings/weighted_DeepWalk/scripts/

For polypharmacy dataset (experiment setting 1):

DeepWalk:

python generate_rand_walks_polypharmacy.py --data_file ../dataset/decagon.csv --output_file polypharmacy_randu --n 1 --num_walks 25 --walk_length 500 --sample_method randu --add_edges

Weighted DeepWalk:

python generate_rand_walks_polypharmacy.py --data_file ../dataset/decagon.csv --output_file polypharmacy_attrs --n 1 --num_walks 25 --walk_length 500 --sample_method attrs --add_edges

For polypharmacy and SemMedDB dataset (experiment setting 2):

DeepWalk:

python generate_rand_walks_combined.py --data_file ../dataset/combined.csv --output_file combined_randu --n 1 --num_walks 375 --walk_length 500 --sample_method randu --add_edges

Weighted DeepWalk:

python generate_rand_walks_combined.py --data_file ../dataset/combined.csv --output_file combined_attrs --n 1 --num_walks 375 --walk_length 500 --sample_method attrs --add_edges
```

###### Stage 2 - Representation Learning:

To generate representations for the dataset, execute the following commands:

```
cd ~/kb_embeddings/weighted_DeepWalk/word2vec-master/

gcc word2vec.c -o word2vec -lm -pthread -O3 -march=native -funroll-loops

For polypharmacy dataset (experiment setting 1):

DeepWalk:

./word2vec -train ../scripts/polypharmacy_randu.0.txt -output polypharmacy_randu -window 10 -size 256 -sample 1e-5 -iter 5 -alpha 0.025 -threads 4

Weighted DeepWalk:

./word2vec -train ../scripts/polypharmacy_attrs.0.txt -output polypharmacy_attrs -window 10 -size 256 -sample 1e-5 -iter 5 -alpha 0.025 -threads 4

For polypharmacy and SemMedDB dataset (experiment setting 2):

DeepWalk:

./word2vec -train ../scripts/combined_randu.0.txt -output combined_randu -window 10 -size 256 -sample 1e-5 -iter 5 -alpha 0.025 -threads 4

Weighted DeepWalk:

./word2vec -train ../scripts/combined_attrs.0.txt -output combined_attrs -window 10 -size 256 -sample 1e-5 -iter 5 -alpha 0.025 -threads 4
```

##### TransE and weighted TransE

To obtain the representations learnt using TransE and weighted TransE, execute the following steps:

```
cd ~/kb_embeddings/weighted_transE/

For polypharmacy dataset (experiment setting 1):

TransE:

python train_transE_polypharmacy.py

Weighted TransE:

python train_weighted_transE_polypharmacy.py

For polypharmacy and SemMedDB dataset (experiment setting 2):

TransE:

python train_transE_combined.py

Weighted TransE:

python train_weighted_transE_combined.py
```

#### Evaluation - Link prediction:

The notebooks to evaluate the representations learnt are available at the location ~/kb_embeddings/experiments/polypharmacy/evaluation. Each notebook evaluates a different representation learnt:

1. polypharmacy_DeepWalk_combined_emb.ipynb - DeepWalk representations from Polypharmacy + SemMed DB dataset

2. Polypharmacy_DeepWalk_lp_Decagon_emb.ipynb - DeepWalk representations from Polypharmacy dataset

3. polypharmacy_weighted_DeepWalk_combined_emb.ipynb - Weighted DeepWalk representations from Polypharmacy + SemMed DB dataset

4. Polypharmacy_weighted_DeepWalk_lp_Decagon_emb.ipynb - Weighted DeepWalk representations from Polypharmacy dataset

5. polypharmacy_TransE_combined_emb.ipynb - TransE representations from Polypharmacy + SemMed DB dataset

6. Polypharmacy_TransE_lp_Decagon_emb.ipynb - TransE representations from Polypharmacy dataset

7. polypharmacy_weighted_TransE_combined_emb.ipynb - Weighted TransE representations from Polypharmacy + SemMed DB dataset

8. Polypharmacy_weighted_TransE_lp_Decagon_emb.ipynb - Weighted TransE representations from Polypharmacy dataset
