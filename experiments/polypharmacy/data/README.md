This folder contains the data used in creating triplets to be used by weighted DeepWalk and weighted TransE.

Reference dataset is obtained from https://zenodo.org/record/3333834/files/esp_ddi_zenodo.tar.gz (from the paper Predicting Adverse Drug-Drug Interactions with Neural Embedding of Semantic Predications, Burkhardt et al.)

