Instructions on reproducing the ADE experiment:
=======================================================

#### Environment Setup:

Run the following commands to set up an environment to run the ADE experiments:

```
conda env create -f environment.yml

conda activate ADEClassifierRepLearnML
```

#### Dataset:

The dataset used here is the same as the one from the paper "Learning predictive models of drug side-effect relationships from distributed representations of literature-derived semantic predications" (Mower et al.). The representation learning is done using the SemMedDB dataset.

Download the SemMedDB dataset to the relevant location as follows:

```
cd ./data/

wget <link to SemMedDB tar file>

wget <link to EUADR-OMOP dataset tar file>

tar -xvf <SemMedDB tar file>

tar -xvf <EUADR-OMOP tar file>
```

#### Data Preparation for representation learning:

##### DeepWalk and weighted DeepWalk:

The SemMedDB dataset is already available as a csv file and does not need any change.

Copy the csv file to the relevant folder as follows:

```
cp -rpf ./data/PREDICATIONS_OCCURS.csv ../../weighted_DeepWalk/dataset/
```

##### TransE and weighted TransE:

TransE and weighted TransE require the files entity2id.txt(maps each entity/node of the triplets to a unique id), relation2id.txt(maps each relation/edge of a triplet to a unique id), train2id.txt(maps each triplet to its corresponding entity and relation id).

For weighted TransE, an additional triplets.csv file is also generated, that maps each triplet to its weight. The weight is calculated as the ratio of (subject score * occurence score * object score of the triplet) to (maximum subject score * maximum object score * maximum occurence score of the dataset).

To generate the relevant files for Polypharmacy dataset, execute the following steps:

1. Open the notebook at the location ./data_prep/weighted_TransE/data_prep_weighted_TransE_semmeddb.ipynb
2. Ensure the path references mentioned in the notebook have all the necessary files.
3. Execute the entire notebook.

The resulting files would be saved at the location ~/kb_embeddings/weighted_transE/benchmarks/semmed/

#### Representation Learning:

##### DeepWalk and weighted DeepWalk:

###### Stage 1 - Random Walk generation:

To generate random walks for the dataset, execute the following commands:

```
cd ~/kb_embeddings/weighted_DeepWalk/scripts/

DeepWalk:

python generate_rand_walks_semmed.py --data_file ../dataset/PREDICATIONS_OCCURS.csv --output_file semmed_randu --n 1 --num_walks 10 --walk_length 100 --sample_method randu --add_edges

Weighted DeepWalk:

python generate_rand_walks_semmed.py --data_file ../dataset/PREDICATIONS_OCCURS.csv --output_file semmed_attrs --n 1 --num_walks 10 --walk_length 100 --sample_method attrs --add_edges
```

###### Stage 2 - Representation Learning:

To generate representations for the dataset, execute the following commands:

```
cd ~/kb_embeddings/weighted_DeepWalk/word2vec-master/

gcc word2vec.c -o word2vec -lm -pthread -O3 -march=native -funroll-loops

DeepWalk:

./word2vec -train ../scripts/semmed_randu.0.txt -output semmed_randu -window 10 -size 256 -sample 1e-5 -iter 5 -alpha 0.025 -threads 4

Weighted DeepWalk:

./word2vec -train ../scripts/semmed_attrs.0.txt -output semmed_attrs -window 10 -size 256 -sample 1e-5 -iter 5 -alpha 0.025 -threads 4
```

##### TransE and weighted TransE

To obtain the representations learnt using TransE and weighted TransE, execute the following steps:

```
cd ~/kb_embeddings/weighted_transE/

TransE:

python train_transE_semmed.py

Weighted TransE:

python train_weighted_transE_semmed.py
```

#### Evaluation - Link prediction:
#### Getting the composite feature vectors:
After generating the entity embeddings using TransE, Weighted TransE, Deepwalk and Weighted Deepwalk, save the embeddings in a numpy file called 'embeddings.npy'. Then, the composite vectors for each drug,disease pair can be constructed using the individual feature vectors feach entity. To do so, open the notebook concatenate_transE.ipynb(for transE) or concatenate_deepWalk.ipynb(for deepWalk) using jupyter notebook and execute the entire notebook. 
#### ADE Classifiers:
Save the composite feature vectors along with the drug,disease pairs and their respective labels ( 1 indicates a relationship exists and 0 indicates no relationship exists) in a file called test_deepwalk.csv or test_transE.csv. Then, to run all the ADE classifiers, open the notebook  evaluate_embeddings.ipynb using jupyter notebook and execute the entire notebook. 
#### TSNE visualization:

The representations learnt by the above methods can be visualized using TSNE. To do so, open the notebook all_tSNE_visualization.ipynb using jupyter notebook and execute the entire notebook. 
